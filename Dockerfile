FROM node:18-alpine

# Create the app directory and set the correct permissions for the 'node' user
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

# Set the working directory for all subsequent commands
WORKDIR /home/node/app

# Copy package.json and package-lock.json (or npm-shrinkwrap.json) into the container
# This is done separately from the source code so that Docker can cache the npm install step
#COPY ./package*.json ./

# Ensure the correct permissions on copied files before switching to the node user
RUN chown -R node:node /home/node/app

# Switch to the 'node' user for the installation and build steps (to avoid using root)
USER node

# Install the dependencies defined in package.json
RUN npm install

# Run the build script (ensure 'build' is defined in your package.json)
#RUN npm run build

# Optionally, add any static configuration (this is just an example)
RUN echo "variableData=Dockerfile-Build" >> .env

# Copy the rest of the application source code into the container
COPY --chown=node:node ./ ./

# Expose the port the application will be running on (default for many Node apps is 3000)
EXPOSE 3000

# Define the command to run your Node.js application (in this case, starting the server)
CMD ["npm", "start"]






#------------------old Dockerfiel---------------
# FROM node:18-alpine

# # create dir
# RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
# WORKDIR /home/node/app

# # build dependencies
# COPY ./package*.json ./
# USER node
# RUN npm install

# # create static configuration for app
# RUN echo "variableData=Dockerfile-Build" >> .env

# # copy in source code
# COPY --chown=node:node ./ ./

# # start express server
# CMD [ "npm", "start" ]